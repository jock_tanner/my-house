# My House

This is a rough plan (3D drawing) of a stick frame house I'm building since the spring of 2023.

The process of building goes in continuous feedback loop, so the plans are the subject to change according to the field results.

## Steps

- [x] Foundation. (15 steel helical piles; done by subcontractor.)
- [x] Foundation bindings. (200×200 beams composed of 50×200×4000 larch planks bolted and screwed together.)
- [x] Floor joists and blocking.
- [x] Raising the 1st floor walls.
- [~] Mounting the central beam.
- [x] Floor joists and blocking.
- [x] Temporary stairs and subfloor.
- [x] Raising the 2nd floor walls.
- [x] Raising the trusses.
- [x] Roof blocking and support.
- [ ] Roof and wall sheathing.
- [ ] Putting the membrane on walls and the roof.
- [ ] Mounting the overhangs.
- [ ] Roofing.
- [ ] …

## Software

This project is done using FreeCAD up to version 0.21.
